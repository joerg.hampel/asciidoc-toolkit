---
title: "New release: v1.2.0"
date: 2020-07-10T11:23:05+02:00
tags: ["release"]
draft: false
---

This version introduces some new features, changes and bug fix you can see below. 

### Release note

#### New features:
* Add Diagram bloc --> [#22](https://gitlab.com/wovalab/open-source/asciidoc-toolkit/-/issues/22)
* Add function to add custom attributes not directly handle by the toolkit --> [#26](https://gitlab.com/wovalab/open-source/asciidoc-toolkit/-/issues/26)
* Handle Doctype property --> [#25](https://gitlab.com/wovalab/open-source/asciidoc-toolkit/-/issues/25)

#### Changes:
* Email information added to Author --> [#23](https://gitlab.com/wovalab/open-source/asciidoc-toolkit/-/issues/23) :warning: This change breaks public toolkit interface. You will have to modify existing code after update


* Wovalab_lib_AsciiDoctor library is no more installed in Sources folder

#### Bug Fix :
* Document header was ill formed when Autors array was empty --> [#24](https://gitlab.com/wovalab/open-source/asciidoc-toolkit/-/issues/24)
<!--more-->

### Package Download

Package is directly available through the free version of [VI Package Manager](https://vipm.jki.net/get).

If you still want to download the package, use this link: [Asciidoc toolkit for LabVIEW v1.2.0](https://wovalab.gitlab.io/open-source/asciidoc-toolkit/vip/wovalab_lib_asciidoc_for_labview-1.2.0.30.vip)

### LabVIEW supported version

2014 (32 and 64 bit) and above

